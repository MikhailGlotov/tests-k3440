//
//  User+CoreDataClass.swift
//  Tests
//
//  Created by Glotov Michael on 16/02/2019.
//  Copyright © 2019 Glotov Michael. All rights reserved.
//
//

import Foundation
import CoreData
import CryptoSwift

@objc(User)
public class User: NSManagedObject {
    
    convenience init(login: String,
                     password: String?,
                     fname: String,
                     name: String,
                     lname: String,
                     birthDate: Date,
                     authKey: String) {
        self.init(context: CoreDataService.shared.viewContext)
        self.login = login
        self.password = password
        self.fname = fname
        self.name = name
        self.lname = lname
        self.isBlocked = false
        self.birthDate = birthDate as NSDate
        self.createdAt = NSDate()
        self.updatedAt = NSDate()
        self.authKey = authKey
    }
    
    static func registerUser(login: String,
                             password: String?,
                             fname: String,
                             name: String,
                             lname: String,
                             birthDate: Date,
                             authKey: String) -> User? {
        
        let newUser = User(login: login,
                           password: (password != nil) ? password?.md5() : User.generatePassword().md5(),
                           fname: fname,
                           name: name,
                           lname: lname,
                           birthDate: birthDate,
                           authKey: authKey)
        return newUser
    }
    
    //Авторизация по ключу
    static func auth(key: String) -> Bool {
        var result = false
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.predicate = NSPredicate(format: "key == %@", key)
        do {
            result = try CoreDataService.shared.viewContext.fetch(request).count > 0
        } catch {
            print(error)
        }
        
        return result
    }
    
    //Авторизация по связке логин-пароль
    static func auth(login: String, password: String) -> Bool {
        var result = false
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.predicate = NSPredicate(format: "login == %@ && password ==%@", login, password.md5())
        do {
            result = try CoreDataService.shared.viewContext.fetch(request).count > 0
        } catch {
            print(error)
        }
        
        return result
    }
    
    //Восстановление пароля
    //Не совсем понятно, что подразумевается под восстановлением пароля, если у нас он лежит хэшируется через md5
    static func restorePassword(for login: String) -> String? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.predicate = NSPredicate(format: "login == %@", login)
        do {
            guard let user = try CoreDataService.shared.viewContext.fetch(request).first as? User, let password = user.password else {
                return nil
            }
            return password
        } catch {
            print(error)
        }
        return nil
    }
    
    //Вспомогательный метод
    private static func fetchUser(using predicate: NSPredicate) -> User? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.predicate = predicate
        do {
            guard let user = try CoreDataService.shared.viewContext.fetch(request).first as? User else {
                return nil
            }
            return user
        } catch {
            print(error)
        }
        return nil
    }
    
    //Генерация пароля
    //MARK: Заметка
    /* Изначально метод был приватным, пришлось сделать его публичным т.к только так модуль тестов может получить к нему доступ. Кроме того написание теста для данного метода выглядит излешними т.к тест testRegistrationWithEmptyPassword косвенно тестирует его.
     */
    static func generatePassword() -> String {
        let len = 8
        let pswdChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        let rndPswd = String((0..<len).compactMap{ _ in pswdChars.randomElement() })
        return rndPswd
    }
    
    //Удаления пользователя
    static func deleteUser(with login: String) -> Bool {
        var result = false
        guard let user = User.fetchUser(using: NSPredicate(format: "login == %@", login)) else {
            return result
        }
        CoreDataService.shared.viewContext.delete(user)
        do {
            try CoreDataService.shared.viewContext.save()
        } catch {
            print("An error occured while saving context \(error.localizedDescription)")
            return result
        }
        result = true
        return result
    }
    
    //Получение возраста пользователя
    func getAge() -> Int? {
        guard let birthDate = birthDate else {
            return 0
        }
        
        let age = Calendar.current.dateComponents([.year], from: birthDate as Date, to: Date()).year!
        return age
    }
    
    //Полное ФИО
    func fullName() -> String {
        return "\(fname!.capitalized) \(name!.capitalized) \(lname!.capitalized)"
    }
    
    //Инициалы
    func getInitials() -> String {
        return "\(fname!.capitalized) \(String(describing: name!.capitalized.first)).\(String(describing: lname!.capitalized.first))."
    }}
