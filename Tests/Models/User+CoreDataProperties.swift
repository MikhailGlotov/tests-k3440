//
//  User+CoreDataProperties.swift
//  Tests
//
//  Created by Glotov Michael on 16/02/2019.
//  Copyright © 2019 Glotov Michael. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var authKey: String?
    @NSManaged public var birthDate: NSDate?
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var email: String?
    @NSManaged public var fname: String?
    @NSManaged public var isBlocked: Bool
    @NSManaged public var lname: String?
    @NSManaged public var login: String?
    @NSManaged public var name: String?
    @NSManaged public var password: String?
    @NSManaged public var updatedAt: NSDate?

}
