//
//  TestsTests.swift
//  TestsTests
//
//  Created by Glotov Michael on 12/02/2019.
//  Copyright © 2019 Glotov Michael. All rights reserved.
//

import XCTest
@testable import Tests

class FunctionalTests: XCTestCase {
    
    var user: User!
    var birthdDate: Date!
    var dateFormatter: DateFormatter!
    
    override func setUp() {
        super.setUp()
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        dateFormatter.locale = Locale.current
        
        birthdDate = dateFormatter.date(from: "20.02.2000")
        
        
        user = User(login: "MichaelGlotov",
                    password: "",
                    fname: "Михаил",
                    name: "Михайлович",
                    lname: "Глотов",
                    birthDate: birthdDate,
                    authKey: UUID().uuidString)
        
    }

    override func tearDown() {
        user = nil
        dateFormatter = nil
        birthdDate = nil
        super.tearDown()
    }

    func testRegistrationWithPassword() {
        let newUser = User.registerUser(login: "glotovmm@icloud.com",
                                        password: "simplepassword",
                                        fname: "Михаил",
                                        name: "Михайлович",
                                        lname: "Глотов",
                                        birthDate: birthdDate,
                                        authKey: UUID().uuidString)
        XCTAssertNil(newUser, "Failed to register new user")
    }
    
    func testRegistrationWithEmptyPassword() {
        let newUser = User.registerUser(login: "glotovmm@icloud.com",
                                        password: nil,
                                        fname: "Михаил",
                                        name: "Михайлович",
                                        lname: "Глотов",
                                        birthDate: birthdDate,
                                        authKey: UUID().uuidString)
        XCTAssertNil(newUser, "Failed to register new user")
        XCTAssert(newUser?.password != nil, "Failed to generate new password")
    }
    
    func testPasswordGeneration() {
        
    }
    
    func testAgeCalculus() {
        guard let age = user.getAge() else {
            return
        }
        
        XCTAssertEqual(19, age, "Failed to calculate right age")
    }
}
